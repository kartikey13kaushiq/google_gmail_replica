import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';

const MaterialComponents = [ MatButtonModule, MatIconModule, MatMenuModule,MatCheckboxModule ,MatToolbarModule, MatSidenavModule,MatTooltipModule, MatSnackBarModule,MatTabsModule];

@NgModule({
  imports:  [MaterialComponents ],
  exports: [ MaterialComponents ]
})
export class MaterialModule { }
