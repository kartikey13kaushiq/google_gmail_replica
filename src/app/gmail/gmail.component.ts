import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-gmail',
  templateUrl: './gmail.component.html',
  styleUrls: ['./gmail.component.css']
})
export class GmailComponent implements OnInit {

opened = true;
show = false;
search;
open1 = true;
checked = false;
message = "Reloading Mails";
action = "...";

  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }
  onSubmit(i){
    console.log(this.search)
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      panelClass: ['mat-toolbar', 'mat-primary']
    });
  }

url = "./assets/logo_gmail_lockup_default_1x.png";
}
export class MenuOverviewExample {}

