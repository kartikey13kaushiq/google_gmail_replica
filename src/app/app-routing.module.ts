import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GmailComponent } from './gmail/gmail.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path:'gmail', component: GmailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [HomeComponent, GmailComponent];